/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 :  
 */
 
#include "neutrino_defs.h"
#include "dwc_otg_dbg.h"

unsigned int isr_no;
void  OS_CPU_IntHandler (void) 
{
	DBG_Error_Print("OS_CPU_IntHandler-------------------->\n");
	while(1);
}

void  OS_CPU_SysTickHandler (void)
{
	DBG_Error_Print("OS_CPU_SysTickHandler-------------------->\n");
	while(1);	
}

void OS_CPU_PendSVHandler(void)
{
	DBG_Error_Print("OS_CPU_SysTickHandler-------------------->\n");
  while(1);

}	
/*****************************************************************************/
/*                                  General ISR                              */
/*****************************************************************************/
/**
 * \brief
 *    This is default ISR function which is going to be executed in case if any
 *  ISR is not registered.
 *
 * \param  none
 * \return none
 */																					    
void
generic_isr(void)
{
    DBG_Warn_Print("ISR %d not registered-------------------->\n",isr_no);
}

/*****************************************************************************/
/*                                  Cortex ISR                               */
/*****************************************************************************/
/**
 * \brief
 *    NMI interrupt service routine
 *
 * \param  none
 * \return none
 */
void
NMI_isr(void)
{
    DBG_Error_Print("NMI isr generated-------------------->\n");
    while(1);
}

/**
 * \brief
 *    HardFault interrupt service routine
 *
 * \param  none
 * \return none
 */
void
HardFault_isr(void)
{
  DBG_Error_Print("HardFault isr generated---------->\n");
  while(1);
}

/**
 * \brief
 *    MemManage interrupt service routine
 *
 * \param  none
 * \return none
 */
void
MemManage_isr(void)
{
    DBG_Error_Print("MemManage isr generated-------------------->\n");
    while(1);
}

/**
 * \brief
 *    BusFault interrupt service routine
 *
 * \param  none
 * \return none
 */
void
BusFault_isr(void)
{
    DBG_Error_Print("BusFault isr generated-------------------->\n");
    while(1);
}

/**
 * \brief
 *    UsageFault interrupt service routine
 *
 * \param  none
 * \return none
 */
void
UsageFault_isr(void)
{
    DBG_Error_Print("UsageFault isr generated-------------------->\n");
    while(1);
}

/**
 * \brief
 *    SVC interrupt service routine
 *
 * \param  none
 * \return none
 */
void
SVC_isr(void)
{
    DBG_Error_Print("SVC isr generated-------------------->\n");
    while(1);
}

/**
 * \brief
 *    interrupt service routine
 *
 * \param  none
 * \return none
 */
void
DebugMon_isr(void)
{
    DBG_Error_Print("DebugMon isr generated-------------------->\n");
    while(1);
}

/**
 * \brief
 *    PendSV interrupt service routine
 *
 * \param  none
 * \return none
 */
void
PendSV_isr(void)
{
    DBG_Error_Print("PendSV isr generated-------------------->\n");
    while(1);
}

/**
 * \brief
 *    SysTick interrupt service routine
 *
 * \param  none
 * \return none
 */
volatile u32 msTicks; // timeTicks counter
void
SysTick_isr(void)
{
	msTicks++;  // increment timeTicks counter
}

/*****************************************************************************/
/*                                SoC ISR Table                              */
/*****************************************************************************/
/**
 * \brief
 *    Second level ISR vector table, It is According to Spec 0.95
 *
 *  ISR Table
 */

void (* global_isr_table[])(void) =
{
    generic_isr, //0: External interrupt input (GPIO09)
    generic_isr, //1: External interrupt input (GPIO10)
    generic_isr, //2: External interrupt input (GPIO11)
    generic_isr, //3: External interrupt input (INT_i)
    generic_isr, //4: I2C slave interrupt
    generic_isr, //5: I2C master interrupt
    generic_isr, //6: SPI slave interrupt
    generic_isr, //7: HSIC general interrupt
    generic_isr, //8: HSIC endpoint interrupt
    generic_isr, //9: MAC LPI exit interrupt
    generic_isr, //10: MAC Power management interrupt
    generic_isr, //11: MAC interrupt to indicate event from LPI, RGMII, Management counters, power management or MAC core.
    generic_isr, //12: MAC interrupt from eMAC Tx DMA channels.
    generic_isr, //13: MAC interrupt from eMAC Rx DMA channels.
    generic_isr, //14: TDM interrupt
    generic_isr, //15: qSPI interrupt
    generic_isr, //16: GDMA CH Interrupt
    generic_isr, //17: GDMA general interrupt
    generic_isr, //18: SHA interrupt
    generic_isr, //19: UART interrupt
    generic_isr, //20: CAN interrupt
    generic_isr, //21: CAN interrupt
    generic_isr, //22: PCIe controller interrupt
    generic_isr, //23: MCU FLAG interrupt (This is set when intr_mcu_flag is non-zero)
    generic_isr, //24: EXT FLAG interrupt (This is set when intr_ext_flag is non-zero)
};

/************************************** EOF *********************************/
