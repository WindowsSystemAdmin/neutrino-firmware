/* ============================================================================
 *   COPYRIGHT @2015
 *
 *   Toshiba America Electronic Components
 *
 *   ALL RIGHTS RESERVED.
 *   UNPUBLISHED ?PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE
 *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE
 *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.
 *
 *   PROJECT:   NEUTRINO
 *
 *   VERSION:   Revision: 2.0
 *
 *   RELEASE:   Preliminary & Confidential
 *   @date      Jan. 20, 2016
 *
 *   SAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND, 
 *   EITHER EXPRESS OR IMPLIED.
 *
 *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS' PRODUCT DESIGN OR APPLICATIONS.
 *   
 *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS
 *   INTENDED FOR REFERENCE USE ONLY.
 *   
 *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR
 *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.
 *
 * ========================================================================= */

 
/*
 *********************************************************************************************************
 *
 * Filename      : ntn_uart.c
 * Programmer(s) : WZ
 *                 
 *********************************************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "common.h"
#include "ntn_uart.h"

/* Private define ------------------------------------------------------------*/
char gSIOTxBuffer[BUFFER_SIZE] = { 0U };

unsigned char gSIORdIndex = 0U;
unsigned char gSIOWrIndex = 0U;
unsigned char fSIO_INT = 0U;
//unsigned char fSIOTxOK = NO;


//==============================================================================
//SUPPORT FUNCTIONS
//==============================================================================
//------------------------------------------------------------------------------
void uart_initialize(unsigned int baudrate)
{
	unsigned int  mclk_freq;
	unsigned int divider, remainder, fraction;
                                                                     
  mclk_freq = 62500000; 										
	
	hw_reg_write32(UART_BASE, UART_ICR, 0x07ff);	/* Clear all interrupt status */
	
	/*
	 * Set baud rate:
	 * IBRD = UART_CLK / (16 * BAUD_RATE)
	 * FBRD = ROUND((64 * MOD(UART_CLK,(16 * BAUD_RATE))) / (16 * BAUD_RATE))
	 */
	divider = mclk_freq / (16 * baudrate);
	remainder = mclk_freq % (16 * baudrate);
	fraction = (8 * remainder / baudrate) >> 1;
	fraction += (8 * remainder / baudrate) & 1;
		
	hw_reg_write32(UART_BASE, UART_IBRD, divider);
	hw_reg_write32(UART_BASE, UART_FBRD, fraction);

	/* Set N, 8, 1, FIFO enable */
	hw_reg_write32(UART_BASE, UART_LCRH, (LCRH_WLEN8 | LCRH_FEN));		// FIFO enabled

	/* Enable UART */
	hw_reg_write32(UART_BASE, UART_CR, (CR_RXE | CR_TXE | CR_UARTEN));		

	/* Enable TX/RX interrupt */
	hw_reg_write32(UART_BASE, UART_IMSC, (IMSC_RX | IMSC_TX | IMSC_RT)); 
}


void uart_send_data(unsigned char *data, unsigned int count)
{
   while(count--)
   {
			while (hw_reg_read32(UART_BASE, UART_FR) & FR_TXFF)
				;
			hw_reg_write32(UART_BASE, UART_DR, *data++);
   }
}

int uart_get_data(unsigned char *data, unsigned int count)
{
	unsigned char inp;
   while(count--)
   {
      //time_out = m3Ticks + UART_TIME_OUT;
      while( (hw_reg_read32(UART_BASE, UART_FR) & FR_RXFE) != 0 )
      {
         
      }
      inp = hw_reg_read32(UART_BASE, UART_DR);
			*data++ = inp;
			while (hw_reg_read32(UART_BASE, UART_FR) & FR_TXFF)
				;
			hw_reg_write32(UART_BASE, UART_DR, inp);
			
			if(inp == 0x0D) break;
   }
	 
   return 0;
}

void  NTN_Ser_Printf (char *format, ...)
{
    char  buffer[255u + 1u];
    va_list   vArgs;
		unsigned char len = 0;

    va_start(vArgs, format);
    vsprintf((char *)buffer, (char const *)format, vArgs);
    va_end(vArgs);

		while (buffer[len++])
		{
			;
		}
    uart_send_data((unsigned char *) buffer, len);
}



