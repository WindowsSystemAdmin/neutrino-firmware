#ifndef __GPIO_H__
#define __GPIO_H__

/* ============================================================================
 *   COPYRIGHT © 2015
 *
 *   Toshiba America Electronic Components
 *
 *   ALL RIGHTS RESERVED.
 *   UNPUBLISHED – PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE
 *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE
 *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.
 *
 *   PROJECT:   NEUTRINO
 *
 *   VERSION:   Revision: 1.0
 *
 *   RELEASE:   Preliminary & Confidential
 *   @date      25 September 2015
 *
 *   EXAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND, 
 *   EITHER EXPRESS OR IMPLIED.
 *
 *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS' PRODUCT DESIGN OR APPLICATIONS.
 *   
 *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS
 *   INTENDED FOR REFERENCE USE ONLY.
 *   
 *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR
 *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.
 *
 * ========================================================================= */

 
/*
 *********************************************************************************************************
 *
 * Filename      : ntn_gpio.h
 * Programmer(s) : WZ
 *                 
 *********************************************************************************************************
 */
 
#define  NTN_GPIO_REG_BASE            (unsigned int)(0x40001200) 
#define  NTN_GPIO_INPUT0              (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0000))
#define  NTN_GPIO_INPUT1              (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0004))
#define  NTN_GPIO_INPUT_ENABLE0       (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0008)) /* 0-31 */
#define  NTN_GPIO_INPUT_ENABLE1       (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x000C)) /* 32-63 */
#define  NTN_GPIO_OUTPUT0             (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0010))
#define  NTN_GPIO_OUTPUT1             (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0014))
		
	

void taec_gpio0_config_output( unsigned int data);
void taec_gpio0_output_data(unsigned int data);

void taec_gpio1_config_output( unsigned int data);
void taec_gpio1_output_data(unsigned int data);

#endif 

