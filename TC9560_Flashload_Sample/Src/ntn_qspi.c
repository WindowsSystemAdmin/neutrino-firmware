/* ============================================================================
 *   COPYRIGHT © 2015
 *
 *   Toshiba America Electronic Components
 *
 *   ALL RIGHTS RESERVED.
 *   UNPUBLISHED – PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE
 *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE
 *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.
 *
 *   PROJECT:   NEUTRINO
 *
 *   VERSION:   Revision: 2.1
 *
 *   RELEASE:   Preliminary & Confidential
 *   @date      Feb. 5, 2016
 *
 *   EXAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND, 
 *   EITHER EXPRESS OR IMPLIED.
 *
 *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS' PRODUCT DESIGN OR APPLICATIONS.
 *   
 *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS
 *   INTENDED FOR REFERENCE USE ONLY.
 *   
 *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR
 *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.
 *
 *   Revision History:
 * 		- 20150920: V0.1-20150920 Initial  development
 * 		- 20150925: V1.0-20150925 Initial release to customer with OS based fw.h
 *    - 20160202: V2.0-20160202 Use OSLess fw.h. Released to customer
 *    - 20160205: V2.1-20160205 Write the FW version and file name infomation at flash: 
                  0x10000220 and 0x10000240 if fw.h file provides such infomation
 * 
 * ========================================================================= */

 
/*
 *********************************************************************************************************
 *
 * Filename      : ntn_qspi.c
 * Programmer(s) : WZ
 *                 
 *********************************************************************************************************
 */

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include "common.h"
#include "ntn_uart.h"
#include "fw.h"

//#define WAIT_USER_UART_INPUT
#undef 	WAIT_USER_UART_INPUT

#define NTN_SPI_REG_BASE										0x40005000	

#define NTN_SPIC_FlshMemMap0_OFFS						0x0000	
#define NTN_SPIC_FlshMemMap1_OFFS						0x0004
#define NTN_SPIC_DirAccCtrl0_OFFS						0x0008	
#define NTN_SPIC_DirAccCtrl1_OFFS						0x000C
#define NTN_SPIC_DirRdCtrl0_OFFS						0x0010	
#define NTN_SPIC_DirRdCtrl1_OFFS						0x0014	

#define NTN_SPIC_PrgAccCtrl0_OFFS						0x0400	
#define NTN_SPIC_PrgAccCtrl1_OFFS						0x0404
#define NTN_SPIC_PrgAccIntEn_OFFS						0x0408
#define NTN_SPIC_PrgAccStat_OFFS						0x040C

#define NTN_SPIC_PriBufDat_Start_OFFS				0x0500  /* 0x500 - 0x507 */
#define NTN_SPIC_SecBufDat_Start_OFFS				0x0600  /* 0x600 - 0x6FF */

/* Winbond SPI commands */
#define WB_WRITE_STATUS_CMD									0x01
#define WB_PAGE_PROGRAM_CMD									0x02
#define WB_READ_STATUS1_CMD									0x05
#define WB_READ_STATUS2_CMD									0x35
#define WB_WRITE_ENABLE_CMD									0x06
#define WB_FAST_READ_CMD										0x0B
#define WB_SECTOR_ERASE_CMD									0x20        /* 4K sector */
#define WB_BLOCK_ERASE_32K_CMD							0x52        /* 32K block */
#define WB_BLOCK_ERASE_64K_CMD							0xD8        /* 64K block */
#define WB_CHIP_ERASE_CMD										0xC7        /* whole chip */
#define WB_MFID_CMD													0x90

/* flash layout */
#define NTN_FLASH_START_OFFSET							0x00000200  /* HW read header data from here */
#define NTN_FW_START_OFFSET									0x00000400 
#define NTN_FW_FLASH_START									(0x10000000 + NTN_FW_START_OFFSET)

/*
*********************************************************************************************************
*                                    LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/


void NTN_SPI_init(void)
{
	unsigned int data;
	
	data  = hw_reg_read32(0x40000000, 0x100C);
	data |=  1 << 8; // enable Chip Select1
	data |=  1 << 9; // enable QSPI;
	hw_reg_write32(0x40000000, 0x100C, data);
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_FlshMemMap0_OFFS, 0x10000011);
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_FlshMemMap1_OFFS, 0x10100011);
	
	//hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_DirRdCtrl0_OFFS, 0xEB0030A8);
	//hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_DirRdCtrl0_OFFS, 0xB001000);
	//hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl0_OFFS, 0x001F0300);
	return;
}

void hw_reg_write08(unsigned int addr_base, unsigned int addr_offs, unsigned int val)
{
	*(unsigned char *)(addr_base + addr_offs) = (val & 0xFF);
}

unsigned int NTN_SPI_read_mfid()
{
	unsigned int temp, status;
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	
	/* Step 1: set data to primary buffer data */
	temp = WB_MFID_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS+4, 0);
	
	temp = (0x5 << 16)   	// PriBufDataByteCnt = 6
				| (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS + 4);
	//printf("SPI: MF_ID = 0x%x\r\n", temp & 0xFFFF);
	return temp & 0xFFFF;
}

unsigned int NTN_SPI_read_status()
{
	unsigned int temp, status, res;
	
	res = 0;
	
	/* Read status 1 */
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	temp = WB_READ_STATUS1_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x1 << 16)    // PriBufDataByteCnt = 1
	     | (1   <<  4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS);
	res |= (temp >> 8) & 0xFF;
	//printf("SPI: status1 = 0x%x\r\n", temp);
	
	/* Read Status2 */
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	temp = WB_READ_STATUS2_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x1 << 16)    // PriBufDataByteCnt = 1
	     | (1   <<  4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS);
	res |= (temp) & 0xFF00;
	//printf("SPI: status2 = 0x%x\r\n", temp);
	return res;
}

	
void NTN_SPI_fast_read(unsigned int phy_addr, unsigned int *data)
{
	unsigned int i, temp;
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_FAST_READ_CMD | ((phy_addr >> 16) & 0xFF) << 8 
													| ((phy_addr >> 8) & 0xFF) << 16 
													| ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
			
	/* set SPIC_PrgAccCtr1 */
	temp = (0x4 << 16)   		// PriBufDataByteCnt = 4
				| (0xFF000000)   	// SecBufDataByteCnt = 255
				| (1   << 5)    	// SecBufEn = 1
				| (1   << 4);   	// PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	for (i = 0; i < 256; i+=4){
		data[i/4] = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_SecBufDat_Start_OFFS + i);
	}

	return;
}

int NTN_SPI_prog_page(unsigned int phy_addr, unsigned char *data)
{
	unsigned int i;
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_WRITE_ENABLE_CMD;  // Write enable
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* Step 2: set data to primary buffer data */
	temp = WB_PAGE_PROGRAM_CMD 	| ((phy_addr >> 16) & 0xFF) << 8 
															| ((phy_addr >> 8) & 0xFF) << 16 
															| ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/* Step 3: set data to secondary buffer data */
	for (i = 0; i < 255; i += 4) 
	{
		temp = data[i+0] | (data[i+1] << 8) | (data[i+2] << 16) | (data[i+3] << 24);
		hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_SecBufDat_Start_OFFS + i, temp);
	}
	
	/* Step 4: set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 3
	     | (0xFF000000)   // SecBufDataByteCnt = 255
		   | (1   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_sector(unsigned int phy_addr)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_WRITE_ENABLE_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (0 << 24)   // SecBufDataByteCnt = 0
		   | (0   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_SECTOR_ERASE_CMD 	| ((phy_addr >> 16) & 0xFF) << 8 
															| ((phy_addr >> 8) & 0xFF) << 16 
															| ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_32k_block(unsigned int phy_addr)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_WRITE_ENABLE_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (0 << 24)   // SecBufDataByteCnt = 0
		   | (0   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_BLOCK_ERASE_32K_CMD | ((phy_addr >> 16) & 0xFF) << 8 
																| ((phy_addr >> 8) & 0xFF) << 16 
																| ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_64k_block(unsigned int phy_addr)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_WRITE_ENABLE_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (0 << 24)   // SecBufDataByteCnt = 0
		   | (0   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_BLOCK_ERASE_64K_CMD | ((phy_addr >> 16) & 0xFF) << 8 
																| ((phy_addr >> 8) & 0xFF) << 16 
																| ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_chip(void)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_WRITE_ENABLE_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_CHIP_ERASE_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}




int NTN_SPI_write_status(unsigned int status)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = WB_WRITE_ENABLE_CMD;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	temp = NTN_SPI_read_status();
	temp |= status;
	
	/* set data to primary buffer data */
	temp = WB_WRITE_STATUS_CMD |(temp & 0xFFFF) << 8 ;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x2 << 16)   // PriBufDataByteCnt = 3-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_DirRdCtrl0_OFFS, 0xEB0030A8);
	
	temp = NTN_SPI_read_status();
	return 0;
}


unsigned int NTN_QSPI_read_mfid()
{
	unsigned int temp, status;
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	
	/* Step 2: set data to primary buffer data */
	temp = 0x94;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
		     | (0 << 24)   // SecBufDataByteCnt = 0
		     | (0   << 5)    // SecBufEn = 1
	       | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS);
	//printf("SPI: MF_ID = 0x%x\r\n", temp & 0xFFFF);
	return temp & 0xFFFF;
}


int NTN_SPI_flash_loader (void)
{
	int i, j, k;
	unsigned int rdata[128];
	int p, cnt; // pages and count
	unsigned int data, diff;
	unsigned char done = 0;
	unsigned char user_input[4];
	unsigned int lastTicks = 0;
 

	data = NTN_SPI_read_mfid();

	while ( !done ) 
	{
		NTN_Ser_Printf("\nPlease select the test number:\n");
		NTN_Ser_Printf("  1. Prog FW to flash\n");
		NTN_Ser_Printf("  2. Compare FW data\n");
		NTN_Ser_Printf("  3. Prog pattern data\n");
		NTN_Ser_Printf("  9. Quit\n");
		
		/* get user input */
		#ifdef WAIT_USER_UART_INPUT
		uart_get_data(user_input, 2);
		#else
		user_input[0] = '1';
		done = 1;
		#endif
		
		switch ( user_input[0] ) 
		{
		case '1':
			/* First, erase the whole chip */
			NTN_Ser_Printf("\nErase the whole chip.");
			NTN_SPI_erase_chip();
			k = 0;
			do
			{
				k++;
				data = NTN_SPI_read_status();
				if ( m3Ticks > lastTicks+200) {
					NTN_Ser_Printf(".");
					lastTicks = m3Ticks;
				}
			} while(data & 0x1);
			NTN_Ser_Printf("\nErase done!\n");
			
			cnt = 0;
			/* program the first block with 0x80 byte for header: start at flash offset: 512 */
			p = 0;
			rdata[0] = NTN_FW_FLASH_START; 
			rdata[1] = (sizeof(fw_data) + 255)/256 * 256*8;  // in bits
			
			for ( i = 2; i < 64; i++) rdata[i] = 0;
			
			#ifdef HAVE_FW_VERSION
			/* let's also write the version information in this free area */
			data = 0;
			for (i = 0; i < (sizeof(fw_ver) + 3)/4 * 4; i++)
			{
				if (i < sizeof(fw_ver) )
				{
					data = (data << 8) | fw_ver[i];
				}
				else 
				{
					data = (data << 8) | 0x00;
				}
				if ( (i+1) % 4 == 0)
				{
					rdata [8+ i/4] = data;  // Start at offset 0x20
					data = 0;
				} 
			}
			
			/* let's also write the filename information in this free area */
			data = 0;
			for (i = 0; i < (sizeof(fw_name) + 3)/4 * 4; i++)
			{
				if (i < sizeof(fw_name) )
				{
					data = (data << 8) | fw_name[i];
				}
				else 
				{
					data = (data << 8) | 0x00;
				}
				if ( (i+1) % 4 == 0)
				{
					rdata [16+ i/4] = data;  // Start at offset 0x40
					data = 0;
				} 
			}
			#endif
			
			NTN_Ser_Printf("\nProgram pages.");
			NTN_SPI_prog_page(NTN_FLASH_START_OFFSET, (unsigned char *)rdata);
			do
			{
				data = NTN_SPI_read_status();
			} while(data & 0x1);
			
				
			for (p = 0; p < (sizeof(fw_data) + 255)/256; p++)
			{
				for ( i = 0; i < 64; i++)
				{			
					if (cnt < sizeof(fw_data)) 
					{
						rdata[i] = fw_data[cnt+0] | (fw_data[cnt+1] << 8)
										| (fw_data[cnt+2] << 16) | (fw_data[cnt+3] << 24);
						cnt += 4;
					}
					else 
					{
						rdata[i] = 0;
					}
				}
				
				NTN_Ser_Printf(".");
				//NTN_Ser_Printf("Program page: %d\n", p);
				NTN_SPI_prog_page(0x100*p + NTN_FW_START_OFFSET, (unsigned char *)rdata);
				
				do
				{
					data = NTN_SPI_read_status();
				} while(data & 0x01);
			}		
			
			NTN_Ser_Printf("\nProgram Done!\n");	
#ifdef WAIT_USER_UART_INPUT
			break;
#endif
			
		case '2':
			cnt = 0;
			j = 0;
			diff = 0;
		NTN_Ser_Printf("\nCompare pages.");
			for (i = 0; i < sizeof(fw_data); i+=4)
			{
				if(i % 256 == 0) 
				{
					j = 0;
					NTN_SPI_fast_read(NTN_FW_START_OFFSET + (i/256)*0x100, rdata);
					NTN_Ser_Printf(".");
				}
				

				data = fw_data[cnt+0] | (fw_data[cnt+1] << 8)
															| (fw_data[cnt+2] << 16) | (fw_data[cnt+3] << 24);
				cnt += 4;
				if ( data != rdata[j++] )
				{
					diff++;
					NTN_Ser_Printf("\nData Differ at: 0x%x\n", i);
				}
			}

			if (diff == 0) 
			{
				NTN_Ser_Printf("\nProg FW is done. All Data Match!\n");
			}			
			break;
			
		case '3':
			/* First, erase the whole chip */
			NTN_Ser_Printf("Erase the whole chip.");
			NTN_SPI_erase_chip();
			k = 0;
			do
			{
				k++;
				data = NTN_SPI_read_status();
				if ( m3Ticks > lastTicks+200) {
					NTN_Ser_Printf(".");
					lastTicks = m3Ticks;
				}
			} while(data & 0x1);
			NTN_Ser_Printf("\nErase done!\n");
			
			cnt = 0;
			do
			{
				data = NTN_SPI_read_status();
			} while(data & 0x1);
			
			for (p = 0; p < 0x1000; p++) /* 1M = 0x100000 */
			{
				for ( j= 0; j < 0x100; j+=4) 
					rdata[j/4] = j+ p*0x100;
				
				NTN_Ser_Printf("Program page: 0x%x\n", p);
				NTN_SPI_prog_page(0x100*p, (unsigned char *)rdata);
				do
				{
						data = NTN_SPI_read_status();
				} while(data & 0x1);
			}		
			
			NTN_Ser_Printf("Program Done!\n");	
			break;
			
		case '9':
			done = 1;
			break;
		
		default:
			break;
		}
	}
	
	NTN_Ser_Printf("Done!\n");
	
	return 0;
}

