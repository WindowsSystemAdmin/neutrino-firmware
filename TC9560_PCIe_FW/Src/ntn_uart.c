/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      25-Oct-2015 : Initial 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "common.h"
#include "ntn_uart.h"

/* Private define ------------------------------------------------------------*/
char gSIOTxBuffer[BUFFER_SIZE] = { 0U };

uint8_t gSIORdIndex = 0U;
uint8_t gSIOWrIndex = 0U;
uint8_t fSIO_INT = 0U;
uint8_t fSIOTxOK = NO;


//==============================================================================
//SUPPORT FUNCTIONS
//==============================================================================
//------------------------------------------------------------------------------
void uart_initialize(void)
{
	unsigned int  mclk_freq;
	unsigned int divider, remainder, fraction;
                                                                     
  mclk_freq = 62500000; 										
	
	hw_reg_write32(UART_BASE, UART_ICR, 0x07ff);	/* Clear all interrupt status */
	
	/*
	 * Set baud rate:
	 * IBRD = UART_CLK / (16 * BAUD_RATE)
	 * FBRD = ROUND((64 * MOD(UART_CLK,(16 * BAUD_RATE))) / (16 * BAUD_RATE))
	 */
	divider = mclk_freq / (16 * UART_BAUDRATE);
	remainder = mclk_freq % (16 * UART_BAUDRATE);
	fraction = (8 * remainder / UART_BAUDRATE) >> 1;
	fraction += (8 * remainder / UART_BAUDRATE) & 1;
		
	hw_reg_write32(UART_BASE, UART_IBRD, divider);
	hw_reg_write32(UART_BASE, UART_FBRD, fraction);

	/* Set N, 8, 1, FIFO enable */
	hw_reg_write32(UART_BASE, UART_LCRH, (LCRH_WLEN8 | LCRH_FEN));		// FIFO enabled

	/* Enable UART */
	hw_reg_write32(UART_BASE, UART_CR, (CR_RXE | CR_TXE | CR_UARTEN));		

	/* Enable TX/RX interrupt */
	hw_reg_write32(UART_BASE, UART_IMSC, (IMSC_RX | IMSC_TX | IMSC_RT)); 
}


void uart_send_data(unsigned char *data, unsigned int count)
{
   while(count--)
   {
			while (hw_reg_read32(UART_BASE, UART_FR) & FR_TXFF)
				;
			hw_reg_write32(UART_BASE, UART_DR, *data++);
   }
}

int uart_get_data(unsigned char *data, unsigned int count)
{
	unsigned char inp;
   while(count--)
   {
      //time_out = m3Ticks + UART_TIME_OUT;
      while( (hw_reg_read32(UART_BASE, UART_FR) & FR_RXFE) != 0 )
      {
         
      }
      inp = hw_reg_read32(UART_BASE, UART_DR);
			*data++ = inp;
			while (hw_reg_read32(UART_BASE, UART_FR) & FR_TXFF)
				;
			hw_reg_write32(UART_BASE, UART_DR, inp);
			
			if(inp == 0x0D) break;
   }
	 
   return 0;
}

void  NTN_Ser_Printf (char *format, ...)
{
    char  buffer[255u + 1u];
    va_list   vArgs;
		unsigned char len = 0;

    va_start(vArgs, format);
    vsprintf((char *)buffer, (char const *)format, vArgs);
    va_end(vArgs);

		while (buffer[len++])
		{
			;
		}
    uart_send_data((unsigned char *) buffer, len);
}



