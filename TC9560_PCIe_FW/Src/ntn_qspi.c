/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      25-Oct-2015 : Initial 
 */


/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include "common.h"
#include "ntn_uart.h"
#if 1
//#include "fw.h"
#else
unsigned char fw_data[] = {0};
#endif


/*
*********************************************************************************************************
*                                    LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/
void NTN_SPI_init(void)
{
	unsigned int data;
	
	data  = hw_reg_read32(0x40000000, 0x100C);
	data |=  1 << 8; // enable Chip Select1
	data |=  1 << 9; // enable QSPI;
	hw_reg_write32(0x40000000, 0x100C, data);
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_FlshMemMap0_OFFS, 0x10000011);
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_FlshMemMap1_OFFS, 0x10100011);
	
	//hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_DirRdCtrl0_OFFS, 0xEB0030A8);
	//hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_DirRdCtrl0_OFFS, 0xB001000);
	//hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl0_OFFS, 0x001F0300);
	return;
}

void hw_reg_write08(unsigned int addr_base, unsigned int addr_offs, unsigned int val)
{
	*(unsigned char *)(addr_base + addr_offs) = (val & 0xFF);
}

unsigned int NTN_SPI_read_mfid()
{
	unsigned int temp, status;
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	
	/* Step 2: set data to primary buffer data */
	temp = 0x90;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS+4, 0);
	
	temp = (0x5 << 16)   // PriBufDataByteCnt = 6
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS + 4);
	//NTN_Ser_Printf("SPI: MF_ID = 0x%x\r\n", temp & 0xFFFF);
	return temp & 0xFFFF;
}

unsigned int NTN_SPI_read_status()
{
	unsigned int temp, status, res;
	
	res = 0;
	
	/* Read status 1 */
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	temp = 0x05;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x1 << 16)    // PriBufDataByteCnt = 1
	     | (1   <<  4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS);
	res |= (temp >> 8) & 0xFF;
	//NTN_Ser_Printf("SPI: status1 = 0x%x\r\n", temp);
	
	/* Read Status2 */
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	temp = 0x05;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x1 << 16)    // PriBufDataByteCnt = 1
	     | (1   <<  4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS);
	res |= (temp) & 0xFF00;
	//NTN_Ser_Printf("SPI: status2 = 0x%x\r\n", temp);
	return res;
}

int NTN_SPI_read_word(unsigned int phy_addr)
{
	int i;
	unsigned int addr, temp, word;
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	word = 0;
	for (i = 0; i < 4; i++)
	{
		addr = phy_addr + i;
		/* set data to primary buffer data */
		temp = 0x02 | ((addr >> 16) & 0xFF) << 8 
								| ((addr >> 8) & 0xFF) << 16 
								| ((addr >> 0) & 0xFF) << 24;
		hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
				
		/* set SPIC_PrgAccCtr1 */
		temp = (0x3 << 16)   // PriBufDataByteCnt = 3
				 | (0 << 24)   // SecBufDataByteCnt = 255
				 | (0   << 5)    // SecBufEn = 1
				 | (1   << 4);   // PriBufEn = 1
		hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
		
		temp |= 1;
		hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
		do {
			temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
		} while ( temp & 0x2);
		
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS + 4);
		word |= (temp & 0xFF) << (8*i);
	}
	//NTN_Ser_Printf("SPI: read word = 0x%x\r\n", word);
	
	return word;
}
	
void NTN_SPI_fast_read(unsigned int phy_addr, unsigned int *data)
{
	unsigned int i, temp;
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x0B | ((phy_addr >> 16) & 0xFF) << 8 
							| ((phy_addr >> 8) & 0xFF) << 16 
							| ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
			
	/* set SPIC_PrgAccCtr1 */
	temp = (0x4 << 16)   // PriBufDataByteCnt = 4
			| (0xFF000000)   // SecBufDataByteCnt = 255
			| (1   << 5)    // SecBufEn = 1
			| (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	for (i = 0; i < 256; i+=4){
		data[i/4] = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_SecBufDat_Start_OFFS + i);
	}

	return;
}

int NTN_SPI_prog_page(unsigned int phy_addr, unsigned char *data)
{
	unsigned int i;
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x06;  // Write enable
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* Step 2: set data to primary buffer data */
	temp = 0x02 | ((phy_addr >> 16) & 0xFF) << 8 
	            | ((phy_addr >> 8) & 0xFF) << 16 
	            | ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/* Step 3: set data to secondary buffer data */
	for (i = 0; i < 255; i += 4) 
	{
		temp = data[i+0] | (data[i+1] << 8) | (data[i+2] << 16) | (data[i+3] << 24);
		hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_SecBufDat_Start_OFFS + i, temp);
	}
	
	/* Step 4: set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 3
	     | (0xFF000000)   // SecBufDataByteCnt = 255
		   | (1   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_sector(unsigned int phy_addr)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x06;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (0 << 24)   // SecBufDataByteCnt = 0
		   | (0   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x20 | ((phy_addr >> 16) & 0xFF) << 8 
	            | ((phy_addr >> 8) & 0xFF) << 16 
	            | ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_32k_block(unsigned int phy_addr)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x06;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (0 << 24)   // SecBufDataByteCnt = 0
		   | (0   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x52 | ((phy_addr >> 16) & 0xFF) << 8 
	            | ((phy_addr >> 8) & 0xFF) << 16 
	            | ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_64k_block(unsigned int phy_addr)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x06;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (0 << 24)   // SecBufDataByteCnt = 0
		   | (0   << 5)    // SecBufEn = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0xD8 | ((phy_addr >> 16) & 0xFF) << 8 
	            | ((phy_addr >> 8) & 0xFF) << 16 
	            | ((phy_addr >> 0) & 0xFF) << 24;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}

int NTN_SPI_erase_chip(void)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x06;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0xC7;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	/*  set SPIC_PrgAccCtr1 */
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	return 0;
}




int NTN_SPI_write_status(unsigned int status)
{
	unsigned int temp;
	
	// Write Enable
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	/* set data to primary buffer data */
	temp = 0x06;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x0 << 16)   // PriBufDataByteCnt = 1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	temp = NTN_SPI_read_status();
	temp |= status;
	
	/* set data to primary buffer data */
	temp = 0x01 |
	       (temp & 0xFFFF) << 8 ;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x2 << 16)   // PriBufDataByteCnt = 3-1
	     | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( temp & 0x2);
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_DirRdCtrl0_OFFS, 0xEB0030A8);
	
	temp = NTN_SPI_read_status();
	return 0;
}


unsigned int NTN_QSPI_read_mfid()
{
	unsigned int temp, status;
	
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS, 0);
	
	/* Step 2: set data to primary buffer data */
	temp = 0x94;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS, temp);
	
	temp = (0x3 << 16)   // PriBufDataByteCnt = 4-1
		     | (0 << 24)   // SecBufDataByteCnt = 0
		     | (0   << 5)    // SecBufEn = 1
	       | (1   << 4);   // PriBufEn = 1
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status & 0x2);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS);
	temp |= 1;
	hw_reg_write32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccCtrl1_OFFS, temp);
	
	do {
		status = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PrgAccStat_OFFS);
	} while ( status != 0x1);
	
	temp = hw_reg_read32(NTN_SPI_REG_BASE, NTN_SPIC_PriBufDat_Start_OFFS);
	//NTN_Ser_Printf("SPI: MF_ID = 0x%x\r\n", temp & 0xFFFF);
	return temp & 0xFFFF;
}

#if 0
int NTN_QSPI_test_main (void)
{
	unsigned int temp;
	
	NTN_SPI_init();
	
	NTN_SPI_write_status(0x200);  // QUAD Enable
	
	temp = NTN_QSPI_read_mfid();
	temp = NTN_SPI_read_mfid();
	
	(void)temp;
	return 0;
}

#endif
