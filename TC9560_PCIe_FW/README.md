Release Date: Aug. 19, 2016 V2.3_20160819
===============================================================================


Introduction:
=============
The folder contains a Keil project, which is the firmware for PCIe standalone case.

Environment:
============
1. You will need to have Keil nVision IDE Version 5 (Version 4 should be OK, Version 5 is tested in TAEC). 
   Evaluation licence should be OK, as the the image size is much smaller than 32k. This project does not
   use any RTOS. It is a simply OSless firmware.
2. Keil ULink-me JTAG connector is used.

Procedures
==========
1. Open the project file, compile it and run it. 
2. a binary file will be generated, and it is located at ./InOut
3. A windows tool may be used to convert the binary into header file format. If your windows environment 
   does not support this tool, please ignore this step.

 Notes
==========
 1. In general, this OSless version of firmware is configured for using HW POR sequence.
    #define NTN_M3POR_4PCIE_ENABLE   			DEF_DISABLED
	
	In case that SW POR sequence is required, it can be enabled 
	#define NTN_M3POR_4PCIE_ENABLE   			DEF_ENABLED
	However, this version of SW Sequence is only with limited test so far.

2. This OSless FW can be configured to test wake on can feature and can message buffering
   by defining the macro NTN_WOC as "-DNTN_WOC" in "Options for Target 'NTN_FW'" -> "C/C++" -> "Misc Controls"

3. Some debugging counters are available:
	#define  NTN_M3_DBG_CNT_START       0x0000C800  // Debugging count SRAM area start address
	/* NTN_M3_DBG_CNT_START + 4*0: 		DMA TX0
	 *   .........................
	 * NTN_M3_DBG_CNT_START + 4*4:  	DMA TX4
	 * NTN_M3_DBG_CNT_START + 4*5:  	MAC LPI/PWR/EVENT
	 * NTN_M3_DBG_CNT_START + 4*6:  	DMA RX0
	 *   .........................
	 * NTN_M3_DBG_CNT_START + 4*11:  	DMA RX5
	 * NTN_M3_DBG_CNT_START + 4*12: 	TDM
	 * NTN_M3_DBG_CNT_START + 4*13:  	CAN
	 * NTN_M3_DBG_CNT_START + 4*14:  	Reserved
	 * NTN_M3_DBG_CNT_START + 4*15:  	M3_MS_Ticks (64 bits)
	 * NTN_M3_DBG_CNT_START + 4*17:  	INTC WDT Half Expiry Count
	 */
	 
